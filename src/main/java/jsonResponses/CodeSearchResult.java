package jsonResponses;


import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import org.aopalliance.reflect.Code;

import java.lang.reflect.Type;
import java.nio.charset.CoderResult;
import java.util.ArrayList;
import java.util.List;

public class CodeSearchResult {

    private String repo;
    private String location;
    private String filename;

    public String getRepo() {
        return repo;
    }

    public void setRepo(String repo) {
        this.repo = repo;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    static class CodeSearchResultDeserializer implements JsonDeserializer<List<CodeSearchResult>> {
        public List<CodeSearchResult> deserialize(JsonElement json, Type typeOfT,
                                                  JsonDeserializationContext context) throws JsonParseException {

            if (json == null)
                return null;
            ArrayList<CodeSearchResult> al = new ArrayList<CodeSearchResult>();
            for (JsonElement e : json.getAsJsonArray()) {


                CodeSearchResult codeSearchResult=new CodeSearchResult();
                codeSearchResult.setFilename(e.getAsJsonObject().get("filename").toString());
                codeSearchResult.setFilename(e.getAsJsonObject().get("location").toString());
                codeSearchResult.setFilename(e.getAsJsonObject().get("repo").toString());

                al.add(codeSearchResult);
            }

            return al;
        }
    }
}
