import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.sun.javaws.exceptions.InvalidArgumentException;
import jsonResponses.CodeSearchResult;
import org.apache.commons.collections.map.MultiValueMap;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.logging.LoggingFeature;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RESTClient {

    public static void main(String[] args) throws Exception {

            MultiValueMap argumentsMap=new MultiValueMap();

            for(String keyVal:args)
            {
                String[] parts = keyVal.split("=",2);
                System.out.println(keyVal);
                System.out.println(parts[0]);
                System.out.println(parts[1]);

                if (parts.length==1)
                    throw new InvalidArgumentException(new String[]{"Arguments should be given in key=value form."});

                argumentsMap.put(parts[0],parts[1]);
            }

            if ((!argumentsMap.containsKey("library"))||(!(argumentsMap.getCollection("library").size() ==1)))
                throw new InvalidArgumentException(new String[]{"Argument list should contain library name in format library=name."});

            HashMap<String,Integer> repoSrcKeys=new HashMap<>();

            repoSrcKeys.put("GoogleCodeSearch",1);
            repoSrcKeys.put("Github",2);
            repoSrcKeys.put("Bitbucket",3);
            repoSrcKeys.put("Sourceforge",4);

            ClientConfig config = new ClientConfig(RESTClient.class);
            config.register(LoggingFeature.class);

            Client client = ClientBuilder.newClient(config);

            String codeSearchAddress="https://searchcode.com/api/codesearch_I/?";

            WebTarget webTarget=client.target(codeSearchAddress).queryParam("q","import "+(String) argumentsMap.getCollection("library").toArray()[0]);

            if (argumentsMap.containsKey("src")){
                for(Object sourceVal: argumentsMap.getCollection("src")){
                    webTarget=webTarget.queryParam("src",String.valueOf(repoSrcKeys.get((String)sourceVal)));
                }
            }else{
                for (Map.Entry<String,Integer> sourceCodePair: repoSrcKeys.entrySet()){
                    webTarget=webTarget.queryParam("src",String.valueOf(sourceCodePair.getValue()));
                }
            }

            String previousResult="";

            List<CodeSearchResult> fullCodeSearchResultsList=new ArrayList<>();

            Integer p=0;

            Gson gson=new GsonBuilder().create();

            Type listType = new TypeToken<ArrayList<CodeSearchResult>>() {}.getType();

            while(true){
                webTarget=webTarget.queryParam("p",null);
                webTarget=webTarget.queryParam("p",p);

                Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
                Response response = invocationBuilder.get();

                String currentResult=response.readEntity(String.class);
                String currentStatus= String.valueOf(response.getStatus());

                System.out.println(webTarget.getUri());
                System.out.println(currentStatus);
                System.out.println(currentResult);

                if ((currentResult=="null")||(currentResult.compareTo(previousResult)==0)){
                    break;
                }{
                    previousResult=currentResult;
                    JsonParser jsonParser=new JsonParser();
                    List<CodeSearchResult> currentCodeSearchResults = gson.fromJson(jsonParser.parse(currentResult).getAsJsonObject().getAsJsonArray("results"), listType);
                    fullCodeSearchResultsList.addAll(currentCodeSearchResults);
                }

                p++;


            }

            int i=1;

            for (CodeSearchResult codeSearchResult: fullCodeSearchResultsList){
                System.out.println(i);
                System.out.println(codeSearchResult.getLocation());
                System.out.println(codeSearchResult.getRepo());
                System.out.println(codeSearchResult.getFilename());
                i++;

            }



    }

}
